import { userKey, usernamesUniqueKey } from '$services/keys';
import { client } from '$services/redis';
import type { CreateUserAttrs, User } from '$services/types';
import { genId } from '$services/utils';

export const getUserByUsername = async (username: string) => {
	// check for existing number representation of userId for given username
	const decimalUserId = await client.zScore(usernamesUniqueKey(), username);
	if (!decimalUserId) {
		throw new Error('User does not exist.');
	}

	// parse back decimalUserId to hex userId
	const userId = decimalUserId.toString(16);
	return getUserById(userId);
};

export const getUserById = async (id: string) => {
	const userStorageKey = userKey(id);
	const user = await client.hGetAll(userStorageKey);

	return deserialize(id, user);
};

export const createUser = async (attrs: CreateUserAttrs) => {
	// username has to be unique
	const isUsernameTaken = await client.zScore(usernamesUniqueKey(), attrs.username);
	if (isUsernameTaken) {
		throw new Error('Username is taken.');
	}

	const userId = genId();

	await Promise.all([
		// store user
		client.hSet(userKey(userId), serialize(attrs)),

		// store username->userId to uniques set
		client.zAdd(usernamesUniqueKey(), {
			value: attrs.username,
			score: parseInt(userId, 16) // userId is 3 bytes hex -> transform to number
		})
	]);

	return userId;
};

const serialize = (user: CreateUserAttrs) => ({
	username: user.username,
	// got hashed ans salted, just before calling `createUser()`
	password: user.password
});

const deserialize = (userId: string, user: Record<string, string>): User => ({
	id: userId,
	username: user.username,
	password: user.password
});
