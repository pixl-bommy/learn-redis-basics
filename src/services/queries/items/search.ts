import { client } from '$services/redis';
import { itemsIndexKey } from '$services/keys';
import { deserialize } from './deserialize';

export const searchItems = async (term: string, size: number = 5) => {
	const preProcessedTerm = term
		.replaceAll(/[^a-z0-9]/gi, '')
		.trim()
		.split(' ')
		.map((part) => (part ? `%${part}%` : ''))
		.filter((part) => part)
		.join(' ');

	// if there is no search string (left), there is nothing to search for
	if (!preProcessedTerm) return [];

	// process search
	const results = await client.ft.search(itemsIndexKey(), preProcessedTerm, {
		LIMIT: {
			from: 0,
			size
		}
	});

	// deserialize each found entry
	return results.documents.map(({ id: itemId, value: item }) => deserialize(itemId, item as any));
};
