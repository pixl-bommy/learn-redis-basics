import { sessionKey } from '$services/keys';
import { client } from '$services/redis';
import type { Session } from '$services/types';

export const getSession = async (id: string) => {
	const session = await client.hGetAll(sessionKey(id));
	return deserialize(id, session);
};

export const saveSession = async (session: Session) => {
	return await client.hSet(sessionKey(session.id), serialize(session));
};

const serialize = (session: Session) => ({
	userId: session.userId,
	username: session.username
});

const deserialize = (sessionId: string, session: Record<string, string>): Session => {
	// empty object? => there is no session => user is not logged in
	if (!Object.keys(session).length) return null;

	return {
		id: sessionId,
		userId: session.userId,
		username: session.username
	};
};
