import { createClient, defineScript } from 'redis';
import { itemByViewsKey, itemKey, itemViewsKey } from '$services/keys';
import { loadScriptFromFile } from './loadScriptFromFile';
import { createIndexes } from './create-indexes';

const client = createClient({
	socket: {
		host: process.env.REDIS_HOST,
		port: parseInt(process.env.REDIS_PORT)
	},
	password: process.env.REDIS_PW,
	scripts: {
		incrementUniqueView: defineScript({
			NUMBER_OF_KEYS: 3,
			SCRIPT: loadScriptFromFile('incrementUniqueView.lua'),
			transformArguments: (itemId: string, userId: string) => [
				itemKey(itemId),
				itemViewsKey(itemId),
				itemByViewsKey(),
				itemId,
				userId
			],
			transformReply: (reply: string) => parseInt(reply)
		}),
		releaseLockIfEquals: defineScript({
			NUMBER_OF_KEYS: 1,
			SCRIPT: loadScriptFromFile('releaseLockIfEquals.lua'),
			transformArguments: (key: string, value: string) => [key, value],
			transformReply: () => undefined
		})
	}
});

client.on('error', (err) => console.error(err));
client.on('connect', async () => {
	try {
		await createIndexes();
	} catch (_) {}
});

client.connect();

export { client };
export type Client = typeof client;
