import { SchemaFieldTypes } from 'redis';
import { itemKey, itemsIndexKey } from '$services/keys';
import { client } from './client';

export const createIndexes = async () => {
	const existingIndexes = await client.ft._list();

    // create index once only
	if (existingIndexes.includes(itemsIndexKey())) return;

	return client.ft.create(
		itemsIndexKey(),
		{
			name: {
				type: SchemaFieldTypes.TEXT
			},
			description: {
				type: SchemaFieldTypes.TEXT
			}
		},
		{
			ON: 'HASH',
			PREFIX: itemKey('')
		}
	);
};
