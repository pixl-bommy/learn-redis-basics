import { readFileSync } from 'node:fs';
import { resolve as resolvePath } from 'node:path';

export const loadScriptFromFile = (filename: string) =>
	readFileSync(resolvePath(`./src/services/redis/scripts/${filename}`), 'utf-8');
