import { itemKey, userLikesKey } from '$services/keys';
import { client } from '$services/redis';
import { getItems } from './items';

export const userLikesItem = async (itemId: string, userId: string) => {
	return await client.sIsMember(userLikesKey(userId), itemId);
};

export const likedItems = async (userId: string) => {
	const likedItemIds = await client.sMembers(userLikesKey(userId));
	return await getItems(likedItemIds);
};

export const likeItem = async (itemId: string, userId: string) => {
	// add unique like for user-item tuple
	const updated = await client.sAdd(userLikesKey(userId), itemId);
	if (!updated) return;

	// if new like, update items like too
	return client.hIncrBy(itemKey(itemId), 'likes', 1);
};

export const unlikeItem = async (itemId: string, userId: string) => {
	// remove unique like for user-item tuple
	const updated = await client.sRem(userLikesKey(userId), itemId);
	if (!updated) return;

	// if like removed, update items like too
	return client.hIncrBy(itemKey(itemId), 'likes', -1);
};

export const commonLikedItems = async (userOneId: string, userTwoId: string) => {
	const intersectItemIds = await client.sInter([userLikesKey(userOneId), userLikesKey(userTwoId)]);
	return await getItems(intersectItemIds);
};
