import { historyBidKey, itemKey, itemsByPriceKey } from '$services/keys';
import { Client, client, withLock } from '$services/redis';
import type { CreateBidAttrs, Bid } from '$services/types';
import { DateTime } from 'luxon';
import { getItem } from './items';

export const createBid = async (attrs: CreateBidAttrs) => {
	const itemIdKey = itemKey(attrs.itemId);

	return withLock(itemIdKey, async (lockedClient: Client) => {
		// get item and check conditions
		const item = await getItem(attrs.itemId);
		if (!item) {
			throw new Error('Item does not exist.');
		}

		if (item.price >= attrs.amount) {
			throw new Error('Bid is too low.');
		}

		if (item.endingAt.diff(DateTime.now()).toMillis() < 0) {
			throw new Error('Item closed to bidding.');
		}

		// create bid history entry
		const bidHistory = serializeHistory(attrs.amount, attrs.createdAt);

		return Promise.all([
			// update item itself
			lockedClient.hSet(itemIdKey, {
				bids: item.bids + 1,
				price: attrs.amount,
				highestBidUserId: attrs.userId
			}),
			// add price history entry
			lockedClient.rPush(historyBidKey(attrs.itemId), bidHistory),
			// add highest price entry
			lockedClient.zAdd(itemsByPriceKey(), { value: item.id, score: attrs.amount })
		]);
	});
};

export const getBidHistory = async (itemId: string, offset = 0, count = 10): Promise<Bid[]> => {
	const startIndex = -1 * offset - count;
	const endIndex = -1 - offset;
	const result = await client.lRange(historyBidKey(itemId), startIndex, endIndex);

	return result.map((stored) => deserialize(stored));
};

const serializeHistory = (amount: number, createdAt: DateTime) =>
	`${amount}:${createdAt.toMillis()}`;

const deserialize = (stored: string) => {
	const [amount, createdAt] = stored.split(':');

	return {
		amount: Number.parseFloat(amount),
		createdAt: DateTime.fromMillis(Number.parseInt(createdAt))
	};
};
