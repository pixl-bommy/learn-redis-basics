-- Increment views counter for an item, but only once for each
-- user viewing the items details page.
--
-- runs:
-- EVALSHA {scriptId}
-- 3
-- itemKey
-- itemViewsKey
-- itemByViewsKey
-- itemId
-- userId
--
-- returns:
-- "1", if new user has viewed items page
-- "0", else
local itemKey = KEYS[1]
local itemViewsKey = KEYS[2]
local itemByViewsKey = KEYS[3]

local itemId = ARGV[1]
local userId = ARGV[2]

local isAdded = redis.call("PFADD", itemViewsKey, userId)

-- if user already viewed this item, do nothing
if isAdded == 0 then return "0" end

-- increment item hash views
redis.call("HINCRBY", itemKey, "views", 1)

-- increment item sorted set views
redis.call("ZINCRBY", itemByViewsKey, 1, itemId)

return "1"
