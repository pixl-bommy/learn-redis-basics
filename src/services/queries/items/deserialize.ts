import type { Item } from '$services/types';
import { DateTime } from 'luxon';

export const deserialize = (itemId: string, item: { [key: string]: string }): Item => {
	if (!Object.keys(item).length) return null;

	return {
		id: itemId,
		description: item.description,
		highestBidUserId: item.highestBidUserId,
		imageUrl: item.imageUrl,
		name: item.name,
		ownerId: item.ownerId,
		price: Number.parseFloat(item.price),
		bids: Number.parseInt(item.bids),
		likes: Number.parseInt(item.likes),
		views: Number.parseInt(item.views),
		createdAt: DateTime.fromMillis(Number.parseInt(item.createdAt)),
		endingAt: DateTime.fromMillis(Number.parseInt(item.endingAt))
	};
};
