# Redis: The Complete Developer's Guide

> udemy course - https://www.udemy.com/share/106BOi3@VbufR2fhMVdB_mqBrN_OMivUa6DdoXt5crJhYIbeV5OZ6hR9Z6jpwqPgll3A5C3UMQ==/

Master Redis v7.0 with hands-on exercises. Includes Modules, Scripting, Concurrency, and Streams!

```shell
# start dev server / app
npm run dev

# start sandbox code
npm run sandbox

# runs dummy content seeder
# !!!configured redis db will be flushed by seeder!!!
npm run seed
```