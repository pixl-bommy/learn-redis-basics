import { itemByEndingAtKey, itemKey, itemByViewsKey, itemsByPriceKey } from '$services/keys';
import { client } from '$services/redis';
import type { CreateItemAttrs } from '$services/types';
import { genId } from '$services/utils';
import { deserialize } from './deserialize';
import { serialize } from './serialize';

export const getItem = async (id: string) => {
	const item = await client.hGetAll(itemKey(id));
	return deserialize(id, item);
};

export const getItems = async (ids: string[]) => {
	// hGetAll for each id
	const commands = ids.map((id) => client.hGetAll(itemKey(id)));

	// pipelined command execution
	const results = await Promise.all(commands);

	// map results to items
	const items = results.map((item, index) => deserialize(ids[index], item));

	return items;
};

export const createItem = async (attrs: CreateItemAttrs, userId: string) => {
	// create unique item id
	const itemId = genId();

	await Promise.all([
		// create item itself
		client.hSet(itemKey(itemId), serialize(attrs)),

		// create related "viewed" entry
		client.zAdd(itemByViewsKey(), { value: itemId, score: 0 }),

		// create related "ending" entry
		client.zAdd(itemByEndingAtKey(), { value: itemId, score: attrs.endingAt.toMillis() }),

		// create related "price" entry
		client.zAdd(itemsByPriceKey(), { value: itemId, score: 0 })
	]);

	return itemId;
};
