export const pageCacheKey = (id: string) => `pagecache#${id}`;
export const sessionKey = (id: string) => `session#${id}`;

export const usernamesUniqueKey = () => `usernames:unique`;

export const userKey = (userId: string) => `users#${userId}`;
export const userLikesKey = (userId: string) => `users#${userId}:likes`;

export const itemByViewsKey = () => `items:views`;
export const itemByEndingAtKey = () => `items:endingAt`;
export const itemKey = (id: string) => `items#${id}`;
export const itemViewsKey = (itemId: string) => `items#${itemId}:views`;
export const historyBidKey = (itemId: string) => `history:bids#${itemId}`;
export const itemsByPriceKey = () => `items:price`;
export const itemsIndexKey = () => `idx:items`;

export const lockByKey = (key: string) => `lock:${key}`;
