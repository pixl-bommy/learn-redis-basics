import { randomBytes } from 'crypto';
import { client, Client } from './client';
import { lockByKey } from '$services/keys';

export const withLock = async <T = any>(key: string, cb: (proxiedClient: Client) => Promise<T>) => {
	// init variables to control retry behavior
	const lockTimeoutMs = 2000;
	const retryDelayMs = 100;
	let retries = 20;

	// generate random value to store at lock key
	const lockKeyContent = randomBytes(6).toString('hex');

	// create the lock key
	const lockKey = lockByKey(key);

	// set up while loop to implement retry behavior
	while (retries >= 0) {
		// decrease retries, to avoid endless loop
		retries--;

		// try to SET NX
		const gotTheLock = await client.set(lockKey, lockKeyContent, {
			// has to be set to write only, if not existing
			NX: true,
			// emergency-delete key if something goes wrong
			PX: lockTimeoutMs
		});

		// someone else has the lock -> pause() and retry
		if (!gotTheLock) {
			await pause(retryDelayMs);
			continue;
		}

		// "we" got the lock
		try {
			const signal = { expired: false };
			setTimeout(() => (signal.expired = true), lockTimeoutMs);

			const proxiedClient = buildClientProxy(lockTimeoutMs);
			const result = await cb(proxiedClient);

			// callback succeeded -> free lock and return result
			await client.releaseLockIfEquals(lockKey, lockKeyContent);
			return result;
		} catch (error) {
			// callback failed -> free lock and re-throw
			await client.releaseLockIfEquals(lockKey, lockKeyContent);
			throw error;
		}
	}
};

const buildClientProxy = (timeoutMs: number) => {
	const startTime = Date.now();

	const handler = {
		get(target: Client, prop: keyof Client) {
			if (Date.now() >= startTime + timeoutMs) {
				throw new Error('Lock has expired.');
			}

			const value = target[prop];
			return typeof value === 'function' ? value.bind(target) : value;
		}
	};

	return new Proxy<Client>(client, handler);
};

const pause = (duration: number) => {
	return new Promise((resolve) => {
		setTimeout(resolve, duration);
	});
};
