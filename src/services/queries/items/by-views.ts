import { itemByViewsKey, itemKey } from '$services/keys';
import { client } from '$services/redis';
import { deserialize } from './deserialize';

export const itemsByViews = async (order: 'DESC' | 'ASC' = 'DESC', offset = 0, count = 10) => {
	let results = (await client.sort(itemByViewsKey(), {
		GET: [
			'#',
			`${itemKey('*')}->name`,
			`${itemKey('*')}->views`,
			`${itemKey('*')}->endingAt`,
			`${itemKey('*')}->price`,
			`${itemKey('*')}->imageUrl`
		],
		BY: 'nosort',
		DIRECTION: order,
		LIMIT: { count, offset }
	})) as string[];

	const items = [];
	while (results.length) {
		const [itemId, name, views, endingAt, price, imageUrl, ...rest] = results;
		items.push(deserialize(itemId, { name, views, endingAt, price, imageUrl }));
		results = rest;
	}

	return items;
};
