import { pageCacheKey } from '$services/keys';
import { cacheableRoutes } from '$services/middlewares';
import { client } from '$services/redis';

export const getCachedPage = (route: string) => {
	if (!cacheableRoutes.includes(route)) return;

	const key = pageCacheKey(route);
	return client.get(key);
};

export const setCachedPage = (route: string, page: string) => {
	if (!cacheableRoutes.includes(route)) return;

	const key = pageCacheKey(route);
	return client.set(key, page, { EX: 2 });
};
