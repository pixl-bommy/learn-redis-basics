import { itemKey, itemsByPriceKey } from '$services/keys';
import { client } from '$services/redis';
import { deserialize } from './deserialize';

export const itemsByPrice = async (order: 'DESC' | 'ASC' = 'DESC', offset = 0, count = 10) => {
	let results = (await client.sort(itemsByPriceKey(), {
		GET: [
			'#',
			`${itemKey('*')}->name`,
			`${itemKey('*')}->views`,
			`${itemKey('*')}->endingAt`,
			`${itemKey('*')}->price`,
			`${itemKey('*')}->imageUrl`
		],
		BY: 'nosort',
		DIRECTION: order,
		LIMIT: { count, offset }
	})) as string[];

	const items = [];
	while (results.length) {
		const [itemId, name, views, endingAt, price, imageUrl, ...rest] = results;
		items.push(deserialize(itemId, { name, views, endingAt, price, imageUrl }));
		results = rest;
	}

	return items;
};
