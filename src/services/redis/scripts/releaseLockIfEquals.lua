-- Deletes a lock-key if stored value equals argurment
--
-- runs:
-- EVALSHA {scriptId}
-- 1
-- lockKey
-- lockValue
local lockKey = KEYS[1]
local lockValue = ARGV[1]

local currentLockValue = redis.call("GET", lockKey)

if lockValue == currentLockValue then redis.call("DEL", lockKey) end
