import { itemByEndingAtKey } from '$services/keys';
import { client } from '$services/redis';
import { getItems } from './items';

export const itemsByEndingTime = async (order: 'DESC' | 'ASC' = 'DESC', offset = 0, count = 10) => {
	// first item should be ending after or just now
	const now = Date.now();

	const itemIds = await client.zRange(itemByEndingAtKey(), now, '+inf', {
		BY: 'SCORE', // <- byscore to filter by timestamps
		LIMIT: { count, offset },
	});

	return getItems(itemIds);
};
